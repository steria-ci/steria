# steria

[![pipeline status](https://gitlab.com/steria-ci/steria/badges/master/pipeline.svg)](https://gitlab.com/steria-ci/steria/-/commits/master)

`steria` - система состоящая из нескольких компонентов,
 которая позволяет узнать информацию о исторических
 зданиях Санкт-Петербурга.
 
- [Документация](doc/README.md)   
- [Разработка](doc/devinfo.md)

[Changelog]](doc/news.mc)


test



## Установка

Сделать активной нужную версию python:

```bash
$ pyenv local
```

Установить зависимости:

```bash
$ poetry install
```

Путь к виртуальному окружению для IDE: 

```bash
$ poetry env info --path
```