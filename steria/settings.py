from pydantic import BaseSettings, Field


class Settings(BaseSettings):
    api_v1: str = '/v1'
    database_url: str = Field('', env='DATABASE_URL')


settings = Settings()
