from fastapi import FastAPI

from steria.api.v1.api import api_router
from steria.settings import settings


def run() -> FastAPI:
    app = FastAPI(
        title="steria server",
        openapi_url="/swagger",
        docs_url="/docs"
    )

    app.include_router(api_router, prefix=settings.api_v1)

    return app
