import os
from subprocess import Popen


def run() -> None:
    cmd = f"uvicorn --port {get_port()} steria.main:run --reload"

    with Popen(cmd.split(' ')) as worker:
        worker.communicate()


def get_port() -> str:
    return os.environ.get('PORT', '5000')


if __name__ == '__main__':
    run()
