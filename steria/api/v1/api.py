from fastapi import APIRouter

from steria.api.v1.endpoints import router

api_router = APIRouter()

api_router.include_router(router, prefix="/debug", tags=["debug"])
