from typing import Any
from fastapi import APIRouter


router = APIRouter()


@router.get("/echo", status_code=200)
def echo(msg: str = "test", sleep_time: int = 0) -> Any:
    """
    Response message
    """
    from time import sleep
    sleep(sleep_time)
    return {"msg": msg.upper()}
