from sqlalchemy import (
    Column,
    Integer,
    String,
    MetaData
)
from sqlalchemy.ext.declarative import declarative_base

# https://docs.sqlalchemy.org/en/13/core/constraints.html#configuring-constraint-naming-conventions
convention = {
    'all_column_names': lambda constraint, table: '_'.join([
        column.name for column in constraint.columns.values()
    ]),
    'ix': 'ix__%(table_name)s__%(all_column_names)s',
    'uq': 'uq__%(table_name)s__%(all_column_names)s',
    'ck': 'ck__%(table_name)s__%(constraint_name)s',
    'fk': 'fk__%(table_name)s__%(all_column_names)s__%(referred_table_name)s',
    'pk': 'pk__%(table_name)s'
}

Base = declarative_base(
    metadata=MetaData(naming_convention=convention)
)


class House(Base):
    __tablename__ = 'house'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', String, nullable=False)
    address = Column('address', String, nullable=False)
    style = Column('style', String, nullable=False)  # use enum
    year = Column('year', String, nullable=False)
    architects = Column('architects', String, nullable=False)
