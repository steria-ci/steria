from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


from steria.settings import settings

engine = create_engine(settings.database_url)
Session = sessionmaker(bind=engine, autoflush=False, autocommit=False)
