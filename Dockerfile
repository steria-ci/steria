FROM python:3.9.2

#RUN adduser -D steria

WORKDIR /home/app

COPY dist dist

RUN pip3 install dist/steria.tar.gz

#RUN chown -R steria:steria ./

#USER steria

#EXPOSE 5000
CMD uvicorn --host 0.0.0.0 --port $PORT steria.main:run
