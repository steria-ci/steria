#!/bin/sh

set -x

curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3

. $HOME/.poetry/env

poetry install --no-interaction

echo '. $HOME/.poetry/env'
